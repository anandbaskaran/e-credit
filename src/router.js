/*=========================================================================================
  File Name: router.js
  Description: Routes for vue-router. Lazy loading is enabled.
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const router = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    scrollBehavior () {
        return { x: 0, y: 0 }
    },
    routes: [
      {
        // =============================================================================
        // MAIN2 LAYOUT ROUTES
        // =============================================================================
        path: "",
        component: () => import("./layouts/main/Main2.vue"),
        children: [
          // =============================================================================
          // Theme Routes
          // =============================================================================
  
          {
            path: "/",
            name: "eCredit",
            component: () => import("./views/Home.vue")
          },
          {
            path: "/home",
            name: "home",
            component: () => import("./views/Home.vue")
          }
        ]
      },

        {
    // =============================================================================
    // MAIN LAYOUT ROUTES
    // =============================================================================
            path: '',
            component: () => import('./layouts/main/Main.vue'),
            children: [
        // =============================================================================
        // Theme Routes
        // =============================================================================
              {
                path: '/sme',
                name: 'sme',
                component: () => import('./views/Sme.vue')
              },{
                path: '/smeCredit',
                name: 'smeCredit',
                component: () => import('./views/SmeCredit.vue')
              },{
                path: '/c-bundle',
                name: 'c-bundle',
                component: () => import('./views/CBundle.vue')
              },
            ],
        },
    // =============================================================================
    // FULL PAGE LAYOUTS
    // =============================================================================
        {
            path: '',
            component: () => import('@/layouts/full-page/FullPage.vue'),
            children: [
        // =============================================================================
        // PAGES
        // =============================================================================
              {
                path: '/pages/login',
                name: 'page-login',
                component: () => import('@/views/pages/Login.vue')
              },
              {
                path: '/pages/error-404',
                name: 'page-error-404',
                component: () => import('@/views/pages/Error404.vue')
              },
            ]
        },
        // Redirect to 404 page, if no match found
        {
            path: '*',
            redirect: '/pages/error-404'
        }
    ],
})

router.afterEach(() => {
  // Remove initial loading
  const appLoading = document.getElementById('loading-bg')
    if (appLoading) {
        appLoading.style.display = "none";
    }
})

export default router
